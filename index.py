from bs4 import BeautifulSoup
import requests
import re
import pandas as pd

npsn = []
alamat = []
desa = []
kecamatan = []
kota = []
provinsi = []
kode_pos = []
jenjang_pendidikan = []
akreditasi = []
nama = []
email = []
website = []

list_npsn = ['69967709', '20501705', '20540054']

# function to extract html document from given url


def getHTMLdocument(url):

    # request for HTML document of given url
    response = requests.get(url)

    # response will be provided in JSON format
    return response.text


for x in list_npsn:
    url_to_scrape = "https://referensi.data.kemdikbud.go.id/tabs.php?npsn="+x
    # create document
    html_document = getHTMLdocument(url_to_scrape)

    html_soup = BeautifulSoup(html_document, 'html.parser')
    all_paragraf = html_soup.find_all('td')

    for i in range(len(all_paragraf)):
        all_paragraf[i] = all_paragraf[i].text

    counter_nama = all_paragraf.index("Nama")
    counter_alamat = all_paragraf.index("Alamat")
    counter_desa = all_paragraf.index("Desa/Kelurahan")
    counter_kecamatan = all_paragraf.index("Kecamatan/Kota (LN)")
    counter_kota = all_paragraf.index("Kab.-Kota/Negara (LN)")
    counter_provinsi = all_paragraf.index("Propinsi/Luar Negeri (LN)")
    counter_kode_pos = all_paragraf.index("Kode Pos")
    counter_jenjang_pendidikan = all_paragraf.index("Jenjang Pendidikan")
    counter_akreditasi = all_paragraf.index("Akreditasi")
    counter_email = all_paragraf.index("Email")
    counter_website = all_paragraf.index("Website")

    npsn.append(x)
    alamat.append(all_paragraf[counter_alamat+2])
    desa.append(all_paragraf[counter_desa+2])
    kecamatan.append(all_paragraf[counter_kecamatan+2])
    kota.append(all_paragraf[counter_kota+2])
    provinsi.append(all_paragraf[counter_provinsi+2])
    kode_pos.append(all_paragraf[counter_kode_pos+2])
    jenjang_pendidikan.append(all_paragraf[counter_jenjang_pendidikan+2])
    akreditasi.append(all_paragraf[counter_akreditasi+2])
    nama.append(all_paragraf[counter_nama+2])
    email.append(all_paragraf[counter_email+2])
    website.append(all_paragraf[counter_website+2])

# importing the module

# creating the DataFrame
cars_data = pd.DataFrame({'npsn': npsn,
                          'alamat': alamat,
                          'desa': desa,
                          'kecamatan': kecamatan,
                          'kota': kota,
                          'provinsi': provinsi,
                          'kode_pos': kode_pos,
                          'jenjang_pendidikan': jenjang_pendidikan,
                          'akreditasi': akreditasi,
                          'nama': nama,
                          'email': email,
                          'website': website
                          })

# writing to Excel
datatoexcel = pd.ExcelWriter('database-sekolah.xlsx')

# write DataFrame to excel
cars_data.to_excel(datatoexcel)

# save the excel
datatoexcel.save()
print('Database sekolah is written to Excel File successfully.')
